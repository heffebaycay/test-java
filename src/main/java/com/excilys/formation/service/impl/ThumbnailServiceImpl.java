package com.excilys.formation.service.impl;

import com.excilys.formation.model.Operation;
import com.excilys.formation.persistence.OperationDao;
import com.excilys.formation.service.ThumbnailService;
import com.excilys.formation.util.ImageUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by loic on 04/09/2014.
 */
@Service
public class ThumbnailServiceImpl implements ThumbnailService {

  private static final Logger logger = LoggerFactory.getLogger(ThumbnailService.class);

  @Value("${images.src.dir}")
  String imagesDirValue;

  @Value("${images.target.dir}")
  String thumbnailsDirValue;

  @Autowired
  OperationDao operationDao;

  File imagesDir;
  File thumbnailsDir;

  @PostConstruct
  private void init() {
    imagesDir = new File(imagesDirValue);
    thumbnailsDir = new File(thumbnailsDirValue);
    logger.debug("Thumbnails dir is: {}", thumbnailsDir.getAbsolutePath());
  }

  @Override
  @Transactional
  public List<String> processImages(int width, int height) {
    clearDirectory(thumbnailsDir);

    StringBuilder sb = new StringBuilder("Image processed: width: ")
        .append(width)
        .append(" height: ").append(height);

    logger.debug(sb.toString());

    return recursiveProcessImages(imagesDir, thumbnailsDir, new Integer(width), new Integer(height));

  }

  @Override
  @Transactional
  public List<String> processImages(int width, int height, int limit) {
    clearDirectory(thumbnailsDir);

    StringBuilder sb = new StringBuilder("Image processed: width: ")
        .append(width)
        .append(" height: ").append(height)
        .append(" limit: ").append(limit);

    logger.debug(sb.toString());

    List<String> thumbnails;

    if (limit == 0)
      thumbnails = recursiveProcessImages(imagesDir, thumbnailsDir, width, height);
    else
      thumbnails = recursiveProcessImages(imagesDir, thumbnailsDir, width, height, limit);

    Operation operation = new Operation();
    operation.setMessage(String.format("Generated %d thumbnails", thumbnails.size()));
    operation.setTime(LocalDateTime.now());

    operationDao.save(operation);

    return thumbnails;

  }

  @Override
  public byte[] getThumbnail(String fileName) {
    File thumbnail = new File(thumbnailsDir, fileName);
    StringBuilder sb = new StringBuilder("Thumbnail retrieved: filePath: ")
        .append(thumbnail.getAbsolutePath());

    logger.debug(sb.toString());

    if (!thumbnail.exists()) {
      logger.warn("Error in getThumbnail: file {} does not exist.", fileName);
      throw new IllegalStateException("Thumbnail file does not exist");
    }

    try {
      return IOUtils.toByteArray(new FileInputStream(thumbnail));
    } catch (IOException e) {
      logger.warn("Error in getThumbnail while reading file {}", fileName);
      throw new IllegalStateException(e);
    }
  }

  private void clearDirectory(File dir) {
    for (File f : dir.listFiles()) {
      if (!f.isFile())
        clearDirectory(f);
      f.delete();
    }
  }

  private List<String> recursiveProcessImages(File root, File thumbnailsDir, Integer width, Integer height) {

    List<String> imageList = new ArrayList<String>();

    for (File f : root.listFiles()) {
      if (!f.isFile()) {
        List<String> subResults = recursiveProcessImages(f, thumbnailsDir, width, height);
        imageList.addAll(subResults);
      } else {
        // f is definitely a file
        if (ImageUtils.isImage(f)) {
          String imageName = ImageUtils.resizeImage(f, thumbnailsDir, width, height);
          imageList.add(imageName);
        }
      }
    }


    return imageList;
  }

  private List<String> recursiveProcessImages(File root, File thumbnailsDir, Integer width, Integer height, Integer limit) {
    logger.debug("Limit value is : {}", limit);
    List<String> imageList = new ArrayList<String>();

    for (File f : root.listFiles()) {

      if (limit == 0) {
        break;
      }

      if (!f.isFile()) {
        List<String> subResults = recursiveProcessImages(f, thumbnailsDir, width, height, limit);
        limit -= subResults.size();
        imageList.addAll(subResults);
      } else {
        // f is definitely a file
        if (ImageUtils.isImage(f)) {
          String imageName = ImageUtils.resizeImage(f, thumbnailsDir, width, height);
          imageList.add(imageName);
          limit--;
          logger.debug("Decremented limit : {}", limit);
        }
      }
    }

    return imageList;
  }

}
