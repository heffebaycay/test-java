package com.excilys.formation.persistence.impl;

import com.excilys.formation.model.Operation;
import com.excilys.formation.persistence.OperationDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by excilys on 07/05/15.
 */
@Repository
public class OperationDaoImpl implements OperationDao {

  private static final Logger logger = LoggerFactory.getLogger(OperationDaoImpl.class);

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  public Operation save(Operation o) {
    if (o == null) {
      throw new IllegalArgumentException("'operation' cannot be null");
    }

    Session session = sessionFactory.getCurrentSession();

    long operationId = (long) session.save(o);

    logger.debug("Saved operation with id {}", operationId);

    o.setId(operationId);


    return o;
  }
}
