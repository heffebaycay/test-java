package com.excilys.formation.controller.impl;

import com.excilys.formation.controller.ThumbnailController;
import com.excilys.formation.dto.ThumbnailRequest;

import com.excilys.formation.dto.ThumbnailResponse;
import com.excilys.formation.service.ThumbnailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class ThumbnailControllerImpl implements ThumbnailController {

  private static final Logger logger = LoggerFactory.getLogger(ThumbnailControllerImpl.class);

  @Autowired
  private ThumbnailService thumbnailService;


  @Override
  @RequestMapping(method = RequestMethod.POST)
  public ModelAndView processImages(@Valid @ModelAttribute("thumbnailDto") ThumbnailRequest thumbnailDto, BindingResult result) {
    logger.debug("processImages!");

    ModelAndView modelAndView;

    if (result.hasErrors()) {
      logger.debug("has error");
      modelAndView = new ModelAndView("index");
      modelAndView.addObject("thumbnailDto", thumbnailDto);

    } else {
      // okay
      logger.debug("Processing images");
      List<String> images = thumbnailService.processImages(thumbnailDto.getWidth(), thumbnailDto.getHeight(), thumbnailDto.getLimit());

      ThumbnailResponse thumbnailResponse = new ThumbnailResponse();
      thumbnailResponse.setThumbnails(images);
      thumbnailResponse.setProcessed(images.size());

      modelAndView = new ModelAndView("result");
      modelAndView.addObject("thumbnailResponse", thumbnailResponse);
    }



    return modelAndView;
  }

  @RequestMapping(method = RequestMethod.GET)
  @Override
  public ModelAndView doGet() {
    ModelAndView modelAndView = new ModelAndView("index");

    modelAndView.addObject("thumbnailDto", new ThumbnailRequest());

    return modelAndView;
  }

}
