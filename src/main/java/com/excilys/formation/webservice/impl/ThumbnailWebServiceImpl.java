package com.excilys.formation.webservice.impl;

import com.excilys.formation.dto.ThumbnailResponse;
import com.excilys.formation.service.ThumbnailService;
import com.excilys.formation.webservice.ThumbnailWebService;
import com.excilys.formation.webservice.exception.BadRequestException;
import com.excilys.formation.webservice.exception.InternalServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/thumbnails")
public class ThumbnailWebServiceImpl implements ThumbnailWebService {

  @Autowired
  private ThumbnailService thumbnailService;

  @Override
  @RequestMapping(value = "/{width}/{height}", method = RequestMethod.POST, produces = "application/json")
  public ThumbnailResponse createThumbnails(@PathVariable int width, @PathVariable int height) {

    if (width < 10 || height < 10) {
      throw new BadRequestException();
    }

    List<String> thumbnails;

    try {
      thumbnails = thumbnailService.processImages(width, height);
    } catch (RuntimeException e) {
      throw new InternalServerErrorException();
    }

    ThumbnailResponse thumbnailResponse = new ThumbnailResponse(thumbnails.size(), thumbnails);

    return thumbnailResponse;
  }

  @Override
  @RequestMapping(value = "/{width}/{height}/{limit}", method = RequestMethod.POST, produces = "application/json")
  public ThumbnailResponse createThumbnails(@PathVariable int width, @PathVariable int height, @PathVariable int limit) {

    if (width < 10 || height < 10 || limit < 0) {
      throw new BadRequestException();
    }

    List<String> thumbnails;

    try {
      thumbnails = thumbnailService.processImages(width, height, limit);
    } catch (RuntimeException e) {
      throw new InternalServerErrorException();
    }

    ThumbnailResponse thumbnailResponse = new ThumbnailResponse(thumbnails.size(), thumbnails);

    return thumbnailResponse;
  }

  @Override
  @RequestMapping(value = "/{fileName:[\\w\\-]+\\.png}", method = RequestMethod.GET, produces = "image/png")
  public byte[] getThumbnail(@PathVariable String fileName) {

    byte[] file;

    try {
      file = thumbnailService.getThumbnail(fileName);
      return file;
    } catch (IllegalStateException e) {
      throw new InternalServerErrorException();
    }

  }
}
