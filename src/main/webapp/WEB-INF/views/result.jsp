<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="include/header.jsp" />

<h1>Thumbnail generator has processed ${thumbnailResponse.processed} items</h1>


<table>

  <thead>
    <tr>
      <th>Thumbnail</th>
      <th>File name</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${thumbnailResponse.thumbnails}" var="thumbnail">
      <tr>
        <td><img src="<c:url value="/thumbnails/${thumbnail}"/>" alt="${thumbnail}"/></td>
        <td>${thumbnail}</td>
      </tr>
    </c:forEach>
  </tbody>

</table>



<jsp:include page="include/footer.jsp" />
