<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="include/header.jsp" />

<h1>Thumbnail generator</h1>


<form:form modelAttribute="thumbnailDto" action="" method="post" id="thumbnailRequestForm">
    <fieldset>
        <div class="form-group">
            <label for="width">Width</label>
            <form:input type="text" id="width" path="width" />
            <form:errors path="width" cssClass="error"></form:errors>
        </div>
        <div class="form-group">
            <label for="height">Height</label>
            <form:input type="text" id="height" path="height" />
            <form:errors path="height" cssClass="error"></form:errors>
        </div>
        <div class="form-group">
            <label for="limit">Limit</label>
            <form:input type="text" id="limit" path="limit" />
            <form:errors path="limit" cssClass="error"></form:errors>
        </div>
        <div class="form-group">
            <input type="submit" value="Process Thumbnails" />
        </div>
    </fieldset>
</form:form>

<jsp:include page="include/footer.jsp" />
